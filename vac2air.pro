;; Wavelength conversion air <-> vacuum
;; VALD3 stores vacuum wavelengths of all the transitions. This allows uniform handling of extraction across the whole spectral range. On the other hand, the selection tools in VALD3 include options for returning the air wavelengths. Furthermore, some of the original line lists include wavelengths measured in the air. This calls for conversion tools. Such tools must be uniformly accurate and reversible across the whole spectral range.

;; For the vacuum to air conversion the formula from Donald Morton (2000, ApJ. Suppl., 130, 403) is used for the refraction index, which is also the IAU standard:

;; n = 1 + 0.0000834254 + 0.02406147 / (130 - s2) + 0.00015998 / (38.9 - s2), where s = 104 / λvac and λvac is in Ångströms.

;; The conversion is then: λair = λvac / n.

;; This formula comes from Birch and Downs (1994, Metrologia, 31, 315) and applies to dry air at 1 atm pressure and 15ºC with 0.045% CO2 by volume. The corrections to Edlén (1953, J. Opt. Soc. Am., 43, 339) are less than 0.0001 Å at 2000 Å and less than 0.001 Å at 30000 Å.

;; The opposite conversion (air-to-vacuum) is less trivial because n depends on λvac and conversion equations with sufficient precision are not readily available. VALD3 tools use the following solution derived by N. Piskunov:

;; n = 1 + 0.00008336624212083 + 0.02408926869968 / (130.1065924522 - s2) + 0.0001599740894897 / (38.92568793293 - s2), where s = 104 / λair and the conversion is: λvac = λair * n.

;; Here is the comparison of the Morton and the inverse transformation by NP between 2000 Å and 100000 Å.

;; The conversions are implemented as a FORTRAN90 module in the functions air2vac and vac2air that take single scalar or vector parameters of type REAL*4 or REAL*8 and return the result of the same type. The module name is AIR_VAC.

;; The corresponding IDL functions are also available.

;http://www.astro.uu.se/valdwiki/Air-to-vacuum%20conversion
function vac2air,wl,reverse=reverse
  
  
  s=1d4/wl
 ; n = 1d + 0.00008336624212083d + 0.02408926869968d / (130.1065924522d - s^2) + 0.0001599740894897d / (38.92568793293d - s^2)
  
  n = 1d + 0.0000834254d + 0.02406147d / (130d - s^2d) + 0.00015998d / (38.9d - s^2d)
  
  if keyword_set(reverse) then retwl=wl * n $
  else retwl=wl / n

  return,retwl
end
