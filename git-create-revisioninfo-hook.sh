#!/bin/sh



#create a git version file
FILENAME='grisred.version'

# get parameters from git
exec 1>&2
branch=`git rev-parse --abbrev-ref HEAD`
shorthash=`git rev-parse --short HEAD`
date=`git log -1 --format='%aD'`
repo=`git config --get remote.origin.url`

# store to file
echo "[$branch]: $shorthash" > $FILENAME 
echo "$date" >> $FILENAME
echo "$repo" >> $FILENAME
