pro show_version
  
  f=file_search('grisred.version',count=cnt)
  
  if cnt eq 1 then begin
    lin='============================================================================'
    spawn,'grep -i push grisred.version',ver1
    spawn,'grep -i commit grisred.version',ver2
    spawn,'grep -i author grisred.version',ver3
    spawn,'grep Date grisred.version',ver4
    mcv=['NOTE: The most recent version maintained by Manolo is here:', $
         'ulises4:/export/home/mcv/idlpro/grisred']
    ver=[lin,'GIT version of the GRIS data reduction routines.',lin,ver1,ver2[0],ver3,ver4, $
        lin,mcv,lin]
  endif else ver='GRIS data reduction routines'
  
  print,ver,format='(a)'
  
end
