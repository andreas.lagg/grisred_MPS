;routine to merge several atlasses and create the fts_combined.fits
;requires a vac2air conversion.
pro fts_merge,show=show
  
; Brault and Neckel (1999SoPh..184..421N)
; https://link.springer.com/article/10.1023%2FA%3A1017165208013
; (converted to fits by HP Doerr)
; According to HP, most accurate atlas available. Only up to WL=12510
  fts1=readfits('fts_neckel_disccentre.fits',h1)
  
  
  dir=['./visatl/','./niratl/','./photatl/']
  pre=['sp','ph','wn']
  order=[[0,2,3,1],[0,1,2,3],[0,1,2,3]]
  specnames=['wn','solar','atmos','total']
  templ={version:1.,datastart:0l,delimiter:32b,missingvalue:!values.f_nan, $
         commentsymbol:'',fieldcount:4l,fieldtypes:[5,4,4,4], $
         fieldnames:specnames, $
         fieldlocations:[0l,12l,25l,38l], $
         fieldgroups:lindgen(4)}
  
  ipos=0l
  inew=0l
  for ia=0,2 do begin
  
    files=file_search(dir[ia]+pre[ia]+'*',count=cnt)
      templ.fieldnames=specnames[order[*,ia]]
    if cnt eq 0 then begin
      print,'No FTS atlas found in '+dir[ia]
      retall
    endif
    
    
    print,'Reading ',dir[ia],cnt,' files, order: ',templ.fieldnames
    for i=0,cnt-1 do begin
;      print,'Reading '+files(i)
      ascii=read_ascii(files(i),template=templ)
      wlvac=1d/ascii.wn*1d8     ;wavelength in Angstroem
      wlair=vac2air(wlvac)
;      Vactoair,wlvac,wlair
      igood=where(wlvac ge 3000. and wlvac le 60000.) ;eliminate bad rows
      n=n_elements(igood)
      dadd=dblarr(5,n)
      if i eq 0 and ia eq 0 then begin
        data=dadd
      endif else data=[[data],[dadd]]
      data(0,ipos:ipos+n-1)=ascii.wn[igood]
      data(1,ipos:ipos+n-1)=wlair[igood]
      data(2,ipos:ipos+n-1)=ascii.solar[igood]
      data(3,ipos:ipos+n-1)=ascii.atmos[igood]
      data(4,ipos:ipos+n-1)=ascii.total[igood]
      if i eq 0 then print,data[*,ipos]
      if i eq cnt-1 then print,data[*,ipos+n-1]
      ipos=ipos+n
    endfor
    inew=[inew,ipos]
  endfor
  
  srt=sort(data[1,*])
  data=data[*,srt]
  

  fits='fts_combined.fits'
  mkhdr,h,data
  sxaddpar,h,'COMMENT','Wallace/Livingston FTS atlas'
  sxaddpar,h,'COMMENT','Combined from visatl, niratl and photatl'
  sxaddpar,h,'INDEX1','wave number in cm^-1'
  sxaddpar,h,'INDEX2','wavelength in Angstrom, converted using IAU standard'
  sxaddpar,h,'INDEX3','spectrum: solar component'
  sxaddpar,h,'INDEX4','spectrum: atmospheric component'
  sxaddpar,h,'INDEX5','spectrum: observed component'
  writefits,fits,data,h
  print,'Wrote FTS spectrum to '+fits
  
  if keyword_set(show) then begin
    wlrg=[[6298.,6306],[10820,10850],[15630,15680]]
    sz=size(wlrg,/dim)
    !p.multi=[0,1,sz[1]] & !p.charsize=1.5
    for iw=0,sz[1]-1 do begin
      inwl=where(data[1,*] ge wlrg[0,iw] and data[1,*] le wlrg[1,iw])
      plot,data[1,inwl],data[2,inwl],/xst
      oplot,data[1,inwl],data[3,inwl],linestyle=1,thick=2
      oplot,data[1,inwl],data[4,inwl],linestyle=2,thick=2
    endfor
    !p.charsize=1. & !p.multi=0
  endif
  stop
  
  
end


