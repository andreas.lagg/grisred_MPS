# Changes for Recalibration at KIS in January 2019

In January 2019 a full recalibration of the GRIS archive was done. This was done to achieve a more homogeneous FITS header structure and to be able to adapt the headers of the Hinode-style split files to comply with [SOLARNET recommendations](http://sdc.uio.no/open/solarnet-20.3). Changes were made to the way headers are written and additional automatization levels were included. ** A major addition, is the fact that there is a .csv file containing all the newest settings for calibration as recommended by KIS are now part of this repository and under verion control. ** A detailed description on how to use the semi automatic reduction pipleine can be found [here](). Changes Made up to this point are:

### Automatization
- Adding __"gris_calibration_settings.csv"__ to store calibration settings in the repository. This way, everything necessary to calibrate data is tracked in one place.
- Adding __"grisred"__ bash script. Usage: `grisred /path/to/day`. Days can be formatted as:
    - `20140426` for a single day
    - `201404{26..27}` for a range of days (both limits included)
    - `201504*` all days for a given month
    - standard unix filename expansion applies
    
    The shell script automatically reduces all specified target directories. If a target does not exist it is skipped. 
    As long as the calibration settings for a given day are included in the .csv file reduction should start automatically,
    as long as the folder name starts with the date formatted as YYYYMMDD and it contains all data in a level0 subfolder.

- Adding __"gen_command.py"__ python script: Generates calibration commands for individual runs from the csv file. Usage: ` python gen_command.py YYYY-MM-DD [run numbers]` 
- Adding __logging__: Reducing gris data with the grisred command produces a `calibration_log.log` logfile in the root directory of the day. This file contains:
     - Timestamp for start of reduction
     - User who performed the reduction
     - Machine where the reduction was performed
     - GIT verisioning information of the reduction
     - Full set of commands used
     - Full log of IDL output (stderr and stdout)
     - Timestamp for end of reduction
- Adding __provenance__: A `HISTORY` block is now appended to each header processed by the gris reduction pipeline. The block includes the date of the reduction as well as versioning information. __Warning: Versioning info is no longer written to the GITREPO, GITREV and GITDATE keywords!__

#### FTS fitting for continuum correction
- Adding __Continuum fit results to header__, introducing __dummy results__ for cases where only one flatfield can be used. This is done to ensure consistency of which keywords are present.
- Adding __storage of continuum fit__: The fit results for continuum fits are stored in a IDL save file `./context_data/20apr15.000_cont_corr.sav`
- Adding __`/lazy` keyword__: Usage in IDL:`gris_v6, target, flatfield, calibration,lambda=lambda,/lazy` prevents the PIKAIA algorithm from performing more than one try on fitting the continuum. We have fond that the fit result usualy does not improve with more iterations.
- Adding __`/quiet` keyword__: Usage in IDL:`gris_v6, target, flatfield, calibration,lambda=lambda,/quiet` prevents the PIKAIA algorithm from stopping the reduction once a sub-par fit result is reached. __Warning: If you use this keyword you need to check the fitness of the continuum fit manually once the reduction has finished!__

### Spectrographic Routines
- Adding newest __routines for spectroscopic measurements__ to the repository, adding __continuum fitting__ to the spectroscopic routines

### Hinode-Style Splitting
- Adding __"gris_split.pro"__ Usage in IDL: `gris_split 'path/to/day/20160520/level1/20may16.007'` Routine to generate Hinode-Style split files. Each split file contains a single slit position for the scan. A new folder `level1_split` is created for the output.

### Minor Changes:
- grisidl script now passes all given parameers to IDL
- Modifying git revision generation for robustness
- Query after sub-par continuum fit now only accepts _y_ and _n_ as valid options
- Improving console output
- Adding some comments and documentation



# V6 changes

gris_v6 mainly handles some errors that have appeared with the derotator positioning angle, required for the polarimetric calibration and telescope correction.

The parameters of the mirrors have been updated and a keyword has been added:

1. The parameters of the mirrors at the beginning of the 2017 campaign were pretty similar to the old values. The difference in the reduced data is hardly noticeable using 2017 or the previous set of parameters. Nevertheless, the main gris routine has been modified to use automatically the old or the new parameter set, depending on the date.

We have three new routines: parameter_mirrors_2016.pro, parameter_mirrors_2017.pro and gris_v6

2. The acquisition program (polar) stores in the header two values of the derotator angle: at the beginning and at the end of each map. This way it can be checked whether the derotator was tracking or not, depending on whether the values are different or not.

There were some time series for which the last value was missing and the reduction routine did not know whether tracking was on or off. It happened that the routine "decided" that in this case the default was "derotator stopped", and this was not true.  To solve this, a new keyword has been introduced in gris_v6: /rotator_track, to force tracking of the derotator during the reduction.

Routine(s) affected: gris_v6

3. Routine gregormod (which calculates the Mueller matrix of the transfer optics) can now handle any angle of the rotator (except for -999). Previosly it only handled angles below 360 degrees 

Routine(s) affected: gregormod_v6

With all this, there are four new routines:

- gris_v6
- gregormod_v6.pro
- parameter_mirrors_2016.pro
- parameter_mirrors_2017.pro

The main routine (gris_v6) can be used for the old data, just by changing gris_v5 by gris_v6 in the cal***.pro routine. There should be no effects

# Manual on git usage

To start working with the git project type:

```git clone https://gitlab.gwdg.de:andreas.lagg/grisred```
if you have access via ssh (e.g. the user tip@ulises4 has that), then: 
```git clone git@gitlab.gwdg.de:andreas.lagg/grisred.git```


The master branch should be used to use the software for data reduction. The following two commands switch to the master branch and update the software:
```
git checkout master
git pull
```
Changes in the reduction package should be done in the development branch. To switch to this branch type:
```
git checkout development
```
It may be that the command complains about the "grisred.version" file, which was changed during the commit process automatically. The easiest solution is to simply delete it, it will be automatically recreated during the nexrt commit.

Everything now do affects only the development branch. You can check in which branch you are in by typing:
```git branch```
(the * should now be at development)

Pull the repository to make sure you work with the most recent version:
```git pull```

To upload changes do the following:
```
git commit -a -m "DESCRIBE WHAT YOU DID"
git push -u origin development
```
Once you think that the development branch should become the official version you type:
```
git pull            #make sure you are up to date
git checkout master #switch back to master branch
git branch          #check that you are now in the master branch
git merge development
   #should merge the development branch with the master one
git push            #as usual. Push to make everything consistent
```