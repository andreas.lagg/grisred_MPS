;function to read in FTS spectrum
function read_fts,hdr=hdr,status=status
  
  status=1
  fits=getenv('FTSFILE')
  fi=file_info(fits)
  if fi.exists eq 0 then begin
    print,'FTS spectrum not found. ' + $
          'Please set the environment variable FTSFILE.'
    print,'Example (bash): '
    print,'export FTSFILE=$HOME/fts_combined.fits'
  endif
  print,'Reading FTS spectrum: ',fits
  ftsfull=readfits(fits,hdr)    
  
  
  status=0
  return, ftsfull
end
